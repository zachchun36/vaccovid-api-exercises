package com.galvanize;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class VaccovidService {

    // This method makes a request to the vaccovid API
    // - GET request (see the `.method("GET", HttpRequest.BodyPublishers.noBody())
    // - endpoint defined with `.uri(URI.create("https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/"))`
    public static HttpResponse<String> getAllCountryCovidData() throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/"))
                .header("X-RapidAPI-Key", "31d26db446mshc4f7f9e97ff2dffp10924ejsnaebfd3ee3f8e")
                .header("X-RapidAPI-Host", "vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com")
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        return HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
    }
}
