package com.galvanize;

import com.google.gson.annotations.SerializedName;

public class CountryData {

    // note: we don't have to have every field defined to match the response. Most de-serialization libraries
    // like Jackson (what Spring uses), Gson, and more will not require you have every field exactly to match
    // your json you're converting into a Java object.  The missing fields will be set to their default values
    // (like 0, null, false, etc.) and the fields that are able to be found will be set.
    private String id;

    // @SerializedName lets us map the JSON property names to our field names without having to match the exact
    //  wording and casing. For example, some JSON property names are underscore case but we typically don't use
    //  underscores for naming fields in Java, so we might want to map it to a different normal name.
    //  In this case, 'Country' was the original JSON property name but we wanted to use 'country'
    @SerializedName("Country")
    private String country;

    @SerializedName("Population")
    private int population;

    @SerializedName("TotalCases")
    private int totalCases;

    @SerializedName("ActiveCases")

    private int activeCases;

    @SerializedName("NewCases")
    private int newCases;

    @SerializedName("TotalRecovered")
    private int totalRecovered;

    @SerializedName("NewRecovered")
    private int newRecovered;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getTotalCases() {
        return totalCases;
    }

    public void setTotalCases(int totalCases) {
        this.totalCases = totalCases;
    }

    public int getActiveCases() {
        return activeCases;
    }

    public void setActiveCases(int activeCases) {
        this.activeCases = activeCases;
    }

    public int getNewCases() {
        return newCases;
    }

    public void setNewCases(int newCases) {
        this.newCases = newCases;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public void setTotalRecovered(int totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public void setNewRecovered(int newRecovered) {
        this.newRecovered = newRecovered;
    }

    @Override
    public String toString() {
        return "CountryData{" +
                "id='" + id + '\'' +
                ", country='" + country + '\'' +
                ", population=" + population +
                ", totalCases=" + totalCases +
                ", activeCases=" + activeCases +
                ", newCases=" + newCases +
                ", totalRecovered=" + totalRecovered +
                ", newRecovered=" + newRecovered +
                '}';
    }
}
