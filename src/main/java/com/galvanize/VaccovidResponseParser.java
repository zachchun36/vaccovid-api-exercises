package com.galvanize;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

public class VaccovidResponseParser {

    public static List<CountryData> parse(HttpResponse<String> response) {
        String responseBody = response.body();

        JsonParser jsonParser = new JsonParser();
        Gson gson = new Gson();

        // Uses JsonParser to convert the JSON response body string into a JsonArray so we can loop over all
        // data or access individual elements
        JsonArray responseArray = jsonParser.parse(responseBody).getAsJsonArray();

        List<CountryData> countryDataList = new ArrayList<>();
        for (JsonElement jsonElement : responseArray) {
            // uses GSON to convert from a single country's String json data to a CountryData object
            CountryData countryData = gson.fromJson(jsonElement.toString(), CountryData.class);

            // normal list stuff, just adding each CountryData we create to a list to keep track of it to return
            countryDataList.add(countryData);
        }
        return countryDataList;
    }
}
