package com.galvanize;

import java.io.IOException;
import java.net.http.HttpResponse;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        HttpResponse<String> response = VaccovidService.getAllCountryCovidData();
        List<CountryData> countryDataList = VaccovidResponseParser.parse(response);

        // System.out.println(countryDataList);
        printXFewestTotalRecoveredFrom(10, countryDataList);
        // printXMostTotalRecoveredFrom(5, countryDataList);
    }

    private static void printXFewestTotalRecoveredFrom(int x, List<CountryData> countryDataList) {
        // Shows an example of sorting the `countryDataList` by the amount of totalRecovered
        List<CountryData> sortedCountryData =
                countryDataList.stream()
                        .sorted(Comparator.comparing(CountryData::getTotalRecovered))
                        .collect(Collectors.toList());

        System.out.println(String.format("---the %d countries with the %d lowest totalRecovered---", x, x));
        // Prints out the x countries with the x lowest totalRecovered
        for (int i = 0; i < x; i++) {
            System.out.println(sortedCountryData.get(i));
        }
        System.out.println();
    }


    // note: could refactor this with above, just being explicit to show different ways to interact with the data
    private static void printXMostTotalRecoveredFrom(int x, List<CountryData> countryDataList) {
        // Shows an example of sorting the `countryDataList` by the amount of totalRecovered
        List<CountryData> sortedCountryData =
                countryDataList.stream()
                        .sorted(Comparator.comparing(CountryData::getTotalRecovered))
                        .collect(Collectors.toList());
        System.out.println(String.format("---the %d countries with the %d highest totalRecovered---", x, x));
        // Prints out the x countries with the x highest totalRecovered
        for (int i = 0; i < x; i++) {
            System.out.println(sortedCountryData.get(sortedCountryData.size() - i - 1));
        }
        System.out.println();
    }


}