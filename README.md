# Contents

In this repository you can find a working demo of getting all the Covid cases data by country from the Vaccovid API.

VaccovidService has a method `getAllCountryCovidData` that makes a request to the Vaccovid API:
- HTTP method: GET
- endpoint: https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/
and the response is returned as a HttpResponse object.

VaccovidResponseParser has a method `parse` that takes in a HttpResponse object and parses the response body as json.
It converts each individual JSON element representing a country into a CountryData object and returns a list of those.

The Main class just uses the above to methods to get all the covid data by country and then interacts with it by sorting it,
printing it out, etc.

# Exploration ideas

basic
- read the code + comments in this project here to understand an example of using an API
- try adding more fields in `CountryData.java` (and add them to the toString() method) to capture more of the response
- try modifying VaccovidResponseParser/CountryData as necessary so that it only returns country data countries located in North America.  
  (this could also be done with a different API operation, but you can also do it in Java to get more comfortable working with the data)
- research more about the Java classes used in this project:
    - HTTP classes: HttpRequest, HttpResponse, HttpClient
    - JSON processing: Gson, JsonParser

advanced
- try to come up with different information you could compile and automate based on the data Vaccovid gives us. 
  For example, maybe you could write code to compute the total number of new cases in all of the countries in North America.  You
  might need to sum up all the values, but you have access to that data.
- try using different Vaccovid API operations.  Choose a different operation from the list on
  the left https://rapidapi.com/vaccovidlive-vaccovidlive-default/api/vaccovid-coronavirus-vaccine-and-treatment-tracker/
  and executing the API request in Java code.  You might have to create a new class to represent the data or change
  the existing fields to capture the response as it's given.
- explore other APIs: https://rapidapi.com/hub 

  